CFLAGS=-Wall -Wextra -pedantic -Werror -g

# To debug with valgrind:
#CFLAGS:=$(CFLAGS) -DVALGRIND

# To get debugging output
#CFLAGS:=$(CFLAGS) -DLF_DEBUG

PROGRAMS=example-uc example-clone
all: $(PROGRAMS)

clean:
	$(RM) *.o $(PROGRAMS) &> /dev/null || true
	
debug: clean
	make "CC=gcc -Wall -pedantic -DLF_DEBUG "

example-uc: libfiber-uc.o example.o
	$(CC) $(LDFLAGS) -pg libfiber-uc.o example.o -o out/example-uc

example-clone: libfiber-clone.o example.o
	$(CC) $(LDFLAGS) -pg libfiber-clone.o example.o -o out/example-clone

libfiber-uc.o: libfiber.h
libfiber-clone.o: libfiber.h
example.o: libfiber.h
