#include <pthread.h>
#include <stdio.h>

#include <unistd.h>

void * fiber1(void *arg)
{
	int i;
	for ( i = 0; i < 100; ++ i )
	{
		printf( "Hey, I'm fiber #1: %d\n", i );
		pthread_yield();
	}
}

void * fibonacchi(void *arg)
{
	int i;
	int fib[2] = { 0, 1 };
	
	/*sleep( 2 ); */
	printf( "fibonacchi(0) = 0\nfibonnachi(1) = 1\n" );
	for( i = 2; i < 50; ++ i )
	{
		int nextFib = fib[0] + fib[1];
		printf( "fibonacchi(%d) = %d\n", i, nextFib );
		fib[0] = fib[1];
		fib[1] = nextFib;
		pthread_yield();
	}
}

void * squares(void *arg)
{
	int i;
	
	/*sleep( 5 ); */
	for ( i = 0; i < 100; ++ i )
	{
		printf( "%d*%d = %d\n", i, i, i*i );
		pthread_yield();
	}
}

int main()
{
	
	int a;
	int b;
	pthread_t threads[2997];
	for (a = 0; a < 1000; a=a + 3) {
		pthread_create(&threads[a], NULL, squares, NULL);
		pthread_create(&threads[a+1], NULL, fibonacchi, NULL);
		pthread_create(&threads[a+2], NULL, fiber1, NULL);
	}

	for (b = 0; b < 2997; b++) {
		pthread_join(threads[b], NULL);
	}

	/* The program quits */
	return 0;
}
