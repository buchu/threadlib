### USAGE

1. Use make to compile all examples
```
$ make
```

2. Use
```
$ gcc -o pthreads pthreads.c -lpthread
```
to compile pthreads example

### MAKING STATIC LIB

1. 
```
$ gcc -c libfiber-clone.c -o libfiber-clone.o
$ gcc -c libfiber-uc.c -o libfiber-uc.o
````
2. 
```
$ ar rcs libfiber-clone_static.a libfiber-clone.o
$ ar rcs libfiber-uc_static.a libfiber-uc.o
```

3. 
```
$ gcc example.o -o example-clone -L. -lfiber-clone_static
$ gcc example.o -o example-uc -L. -lfiber-uc_static
```